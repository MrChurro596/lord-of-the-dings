// Created by iWeb 3.0.4 local-build-20150305

function writeMovie1()
{
    detectBrowser();
    if (windowsInternetExplorer)
    {
        document.write('<object id="id4" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="345" height="210" style="height: 210px; left: 889px; position: absolute; top: 518px; width: 345px; z-index: 1; "><param name="src" value="Media/IMG_0102.mov" /><param name="controller" value="true" /><param name="autoplay" value="false" /><param name="scale" value="tofit" /><param name="volume" value="100" /><param name="loop" value="false" /></object>');
    }
    else if (isiPhone)
    {
        document.write('<object id="id4" type="video/quicktime" width="345" height="210" style="height: 210px; left: 889px; position: absolute; top: 518px; width: 345px; z-index: 1; "><param name="src" value="Home_files/IMG_0102-1.jpg"/><param name="target" value="myself"/><param name="href" value="../Media/IMG_0102.mov"/><param name="controller" value="true"/><param name="scale" value="tofit"/></object>');
    }
    else
    {
        document.write('<object id="id4" type="video/quicktime" width="345" height="210" data="Media/IMG_0102.mov" style="height: 210px; left: 889px; position: absolute; top: 518px; width: 345px; z-index: 1; "><param name="src" value="Media/IMG_0102.mov"/><param name="controller" value="true"/><param name="autoplay" value="false"/><param name="scale" value="tofit"/><param name="volume" value="100"/><param name="loop" value="false"/></object>');
    }
}
setTransparentGifURL('Media/transparent.gif');
function applyEffects()
{
    var registry = IWCreateEffectRegistry();
    registry.registerEffects({stroke_0: new IWStrokeParts([{rect: new IWRect(-2, 2, 4, 191), url: 'Home_files/stroke.png'}, {rect: new IWRect(-2, -2, 4, 4), url: 'Home_files/stroke_1.png'}, {rect: new IWRect(2, -2, 676, 4), url: 'Home_files/stroke_2.png'}, {rect: new IWRect(678, -2, 4, 4), url: 'Home_files/stroke_3.png'}, {rect: new IWRect(678, 2, 4, 191), url: 'Home_files/stroke_4.png'}, {rect: new IWRect(678, 193, 4, 5), url: 'Home_files/stroke_5.png'}, {rect: new IWRect(2, 193, 676, 5), url: 'Home_files/stroke_6.png'}, {rect: new IWRect(-2, 193, 4, 5), url: 'Home_files/stroke_7.png'}], new IWSize(680, 196))});
    registry.applyEffects();
}
function hostedOnDM()
{
    return false;
}
function onPageLoad()
{
    loadMozillaCSS('Home_files/HomeMoz.css')
    adjustLineHeightIfTooBig('id1');
    adjustFontSizeIfTooBig('id1');
    adjustLineHeightIfTooBig('id2');
    adjustFontSizeIfTooBig('id2');
    adjustLineHeightIfTooBig('id3');
    adjustFontSizeIfTooBig('id3');
    adjustLineHeightIfTooBig('id5');
    adjustFontSizeIfTooBig('id5');
    adjustLineHeightIfTooBig('id6');
    adjustFontSizeIfTooBig('id6');
    adjustLineHeightIfTooBig('id7');
    adjustFontSizeIfTooBig('id7');
    adjustLineHeightIfTooBig('id8');
    adjustFontSizeIfTooBig('id8');
    adjustLineHeightIfTooBig('id9');
    adjustFontSizeIfTooBig('id9');
    Widget.onload();
    fixupAllIEPNGBGs();
    fixAllIEPNGs('Media/transparent.gif');
    applyEffects()
}
function onPageUnload()
{
    Widget.onunload();
}
