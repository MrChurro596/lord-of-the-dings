// Created by iWeb 3.0.4 local-build-20150305

setTransparentGifURL('Media/transparent.gif');
function applyEffects()
{
    var registry = IWCreateEffectRegistry();
    registry.registerEffects({stroke_0: new IWStrokeParts([{rect: new IWRect(-2, 2, 4, 191), url: 'Contact_Us_files/stroke.png'}, {rect: new IWRect(-2, -2, 4, 4), url: 'Contact_Us_files/stroke_1.png'}, {rect: new IWRect(2, -2, 676, 4), url: 'Contact_Us_files/stroke_2.png'}, {rect: new IWRect(678, -2, 4, 4), url: 'Contact_Us_files/stroke_3.png'}, {rect: new IWRect(678, 2, 4, 191), url: 'Contact_Us_files/stroke_4.png'}, {rect: new IWRect(678, 193, 4, 5), url: 'Contact_Us_files/stroke_5.png'}, {rect: new IWRect(2, 193, 676, 5), url: 'Contact_Us_files/stroke_6.png'}, {rect: new IWRect(-2, 193, 4, 5), url: 'Contact_Us_files/stroke_7.png'}], new IWSize(680, 196))});
    registry.applyEffects();
}
function hostedOnDM()
{
    return false;
}
function onPageLoad()
{
    loadMozillaCSS('Contact_Us_files/Contact_UsMoz.css')
    adjustLineHeightIfTooBig('id1');
    adjustFontSizeIfTooBig('id1');
    adjustLineHeightIfTooBig('id2');
    adjustFontSizeIfTooBig('id2');
    Widget.onload();
    fixupAllIEPNGBGs();
    fixAllIEPNGs('Media/transparent.gif');
    applyEffects()
}
function onPageUnload()
{
    Widget.onunload();
}
